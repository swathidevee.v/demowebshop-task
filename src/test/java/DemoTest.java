import java.time.Duration;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

import com.demopage.Books;
import com.demopage.Demo_Login;
import com.demopage.Demo_basepage;
import com.demopage.Demo_giftcard;
import com.demopage.Electronics_page;
import com.demopage.Logout_page;


public class DemoTest extends Demo_basepage{
	WebDriver drive;

@Test(priority = 1)
public void Login() {
	driver.manage().window().maximize();
	  driver.get("https://demowebshop.tricentis.com/");
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(7));
		Demo_Login log = PageFactory.initElements(driver, Demo_Login.class);
		log.home_Login();
		log.userId();
		log.userPass();
		log.LoginBtn();
		
}
@Test(priority = 2)
public void books() {
	Books b1 = PageFactory.initElements(driver, Books.class);

b1.books1();

b1.sort();
b1.bookNo1();
b1.bookNo2();
}


@Test(priority = 3)
public void Electronics() {
	Electronics_page e1 = PageFactory.initElements(driver,Electronics_page.class);
e1.electronic1();

e1.cellphone_cart();
e1.cart_btn();
e1.cart1();

}
@Test(priority = 4)
public void Gift_Card() throws InterruptedException {
	Demo_giftcard d1 = PageFactory.initElements(driver,Demo_giftcard .class);
	d1.Contbtn();
d1.giftCard();
d1.cardDisplayed_Per_Page();
d1.DetailsOfCard();

}


@Test(priority = 5)
public void Log_out() {
	Logout_page l1 = PageFactory.initElements(driver,Logout_page .class);
l1.logoutclick();
l1.loginnew();
}







}
