package com.demostep;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import com.demopage.Books;
import com.demopage.Demo_Login;
import com.demopage.Demo_giftcard;
import com.demopage.Electronics_page;
import com.demopage.Logout_page;
import com.demogenerics.*;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class Demo_step {
	public static WebDriver driver;
	
	Demo_Login log = new Demo_Login();
	Books b1 = new Books();
	Electronics_page e1 = new Electronics_page();
	Demo_giftcard d1 = new Demo_giftcard();
	Logout_page l1 = new Logout_page();
	
	
	
	
	
	@Given("open the browser")
	public void open_the_browser() {
		Demo_DriverUtils.getdriver().get("https://demowebshop.tricentis.com/");
	}

	@When("The user clicks on login button available on the home page")
	public void the_user_clicks_on_login_button_available_on_the_home_page() {
		log.home_Login();
		
		
	}

	@When("under returning customer user enters id <{string}> and password <{string}>")
	public void under_returning_customer_user_enters_id_and_password(String string, String string2) {
		log.userId();
		log.userPass();
	}

	@Then("click on login button")
	public void click_on_login_button() {
		log.LoginBtn();
	}
	@When("User select  book category")
	public void user_select_book_category() {
		b1.books1();
	}

	@When("user click on sort by High to low")
	public void user_click_on_sort_by_high_to_low() {
		b1.sort();
	}

	@Then("user selects any two books")
	public void user_selects_any_two_books() {
		b1.bookNo1();
		
	}

	@Then("User adds it to cart")
	public void user_adds_it_to_cart() {
		b1.bookNo2();
	}

	
	

	
	

	@Given("The user navigates to electronic category")
	public void the_user_navigates_to_electronic_category() {
		e1.electronic1();
	}

	@When("The user clicks on cellphone")
	public void the_user_clicks_on_cellphone() {
		e1.cellphone_cart();
	}

	@When("User adds cellphone to cart")
	public void user_adds_cellphone_to_cart() {
		e1.cart_btn();
	}

	@When("User opens shopping cart and checks the total item in cart")
	public void user_opens_shopping_cart_and_checks_the_total_item_in_cart() throws InterruptedException {
		e1.cart1();
		Thread.sleep(3000);
	}
	

	
	
	
	
	@Given("The user navigates to giftcard category")
	public void the_user_navigates_to_giftcard_category() throws InterruptedException {
	d1.Contbtn();
		Thread.sleep(3000);
		d1.giftCard();
	}

	@Then("User selects four to be displayed")
	public void user_selects_four_to_be_displayed() {
		d1.cardDisplayed_Per_Page();
	}

	@Then("User select a gift card and fetch its product details.")
	public void user_select_a_gift_card_and_fetch_its_product_details() {
		d1.DetailsOfCard();
	}

	@Given("User clicks on logout button")
	public void user_clicks_on_logout_button() {
		l1.logoutclick();
	
	}

	@Then("User verifies after logout login is displayed in home page.")
	public void user_verifies_after_logout_login_is_displayed_in_home_page() {
		l1.loginnew();
	}

}
