package com.demopage;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

public class Books extends Demo_basepage{
	@FindBy(partialLinkText = "Books")
	private WebElement book;
	
	
	@FindBy(id="products-orderby")
	private WebElement orderby;

@FindBy(xpath = "(//input[@value='Add to cart'])[1]")
private WebElement cart1;

@FindBy(xpath = "(//input[@value='Add to cart'])[2]")
private WebElement cart2;


public void books1() {
	book.click();
}
public void sort() {
	Select sc=new Select(orderby);
	sc.selectByVisibleText("Price: High to Low");


}

public void bookNo1() {
	cart1.click();
}

public void bookNo2() {
	cart2.click();
}

}






