package com.demopage;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

public class Logout_page extends Demo_basepage {
	@FindBy(xpath = "//a[contains(text(),'Log out')]")
	public WebElement logoutbtn;
	@FindBy(partialLinkText = "Log in")
	public WebElement login2;
	
	public void logoutclick() {
		logoutbtn.click();
	}
	
	public void loginnew() {
		Assert.assertEquals(true,login2.isDisplayed());
		System.out.println("Passed");
	}
	
	
}
