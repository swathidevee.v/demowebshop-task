package com.demopage;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

public class Demo_giftcard extends Demo_basepage {
	@FindBy(xpath="(//input[@type='submit'])[3]")
	private WebElement Cont;
	
	
	@FindBy(partialLinkText = "Gift Cards")
	private WebElement GiftCard;
	@FindBy(id = "products-pagesize")
	private WebElement CardperPage;
	@FindBy(xpath = "//div[@data-productid='1']//div[@class='details']")
	private WebElement Details;
	
	public void Contbtn(){
		
		Cont.click();
		}
	
	
	
	public void giftCard() throws InterruptedException{
		
	GiftCard.click();
	}
	public void cardDisplayed_Per_Page() {
	Select s= new Select(CardperPage);
	s.selectByVisibleText("4");
	}
	public void DetailsOfCard() {
	String det= Details.getText();
	System.out.println("Details of first card :"+det);


	}
}