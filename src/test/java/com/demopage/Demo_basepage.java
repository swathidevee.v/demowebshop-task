package com.demopage;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import com.demogenerics.*;

public class Demo_basepage {
public static WebDriver driver;
	
	public Demo_basepage() {
		driver=Demo_DriverUtils.getdriver();
		PageFactory.initElements(driver, this);
	}
}
