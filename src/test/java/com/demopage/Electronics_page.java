package com.demopage;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

public class Electronics_page extends Demo_basepage {
	@FindBy(xpath = "//ul[@class='top-menu']//a[contains(text(),'Electronics')]")
	private WebElement electronic;

	@FindBy(xpath="//ul[@class='top-menu']//a[contains(text(),'Cell phones')]")
private WebElement cellphone;	
	
	@FindBy(xpath = "(//input[@value='Add to cart'])[1]")
	private WebElement cart3;
	
	@FindBy(linkText = "Shopping cart")
	private WebElement carticon;
	
	@FindBy(className = "cart-qty")
	private WebElement cartNo;

public void electronic1() {
	Actions ac=new Actions(driver);
	ac.moveToElement(electronic).build().perform();
	ac.moveToElement(cellphone).click().perform();
}




public void cellphone_cart() {
	cart3.click();
}

public void cart_btn() {
	carticon.click();
}

public void cart1() {
	String text = cartNo.getText();
	System.out.println("Total No.of Item in cart"+text);
}


}