Feature: DemoWebShop Automation

  Scenario: Login Demowebshop
    Given open the browser
    When The user clicks on login button available on the home page
    And under returning customer user enters id <"mailid"> and password <"password">
    Then click on login button

  Scenario: Select two books and add to cart
    When User select  book category
    And user click on sort by High to low
    Then user selects any two books
    Then User adds it to cart

  Scenario: Under electronics add cellphone to cart
    Given The user navigates to electronic category
    When The user clicks on cellphone
    And User adds cellphone to cart
    And User opens shopping cart and checks the total item in cart

  Scenario: select giftcard and print its details
    Given The user navigates to giftcard category
    Then User selects four to be displayed
    And User select a gift card and fetch its product details.

  Scenario: Logout Demowebshop
    Given User clicks on logout button
    Then User verifies after logout login is displayed in home page.
